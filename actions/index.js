import {
    RESET,
    TAB_CHANGE,
    PRIVATE_CONDITION_CHANGE,
    UPDATE_MILEAGE_CHANGE,
    MILEAGE_CHANGED_BY_USER,
    DEALER_CONDITION_CHANGE,
    TRADE_IN_CONDITION_CHANGE,
    EXPLAINER_MODAL_SHOW,
    EXPLAINER_CONDITION_CHANGE,
    STEP_CHANGE,
    COLLECTED_DETAILS_CHANGE,
    VEHICLE_DETAILS_CHANGE,
    WRONG_PLATE_CHANGE,
    DISABLE_NEXT_STEP,
    VALIDATE_INPUT,
    TOUCH_INPUT,
    IS_MOBILE_CHANGE,
    PARTENER_CHANGE,
    THRESHOLD_CHANGE
} from './types';

//Valuation form
export function stepChange(step) {
    return {
        type: STEP_CHANGE,
        payload: step
    };
}

export function isMobileChange(isMobile) {
    return {
        type: IS_MOBILE_CHANGE,
        payload: isMobile
    };
}

export function updateMileageChange(bool) {
    return {
        type: UPDATE_MILEAGE_CHANGE,
        payload: bool
    };
}

export function partenerChange(partener) {
    return {
        type: PARTENER_CHANGE,
        payload: partener
    };
}

export function touchInput(inputName) {
    return {
        type: TOUCH_INPUT,
        payload: inputName
    };
}

export function disableNextStep(disable) {
    return {
        type: DISABLE_NEXT_STEP,
        payload: disable
    };
}

export function wrongPlateChange(isValid) {
    return {
        type: WRONG_PLATE_CHANGE,
        payload: isValid
    };
}

export function collectedDetailsChange(key, value) {
    return {
        type: COLLECTED_DETAILS_CHANGE,
        payload: {
            key,
            value
        }
    };
}

export function vehicleDetailsChange(details) {
    return {
        type: VEHICLE_DETAILS_CHANGE,
        payload: details
    };
}

export function validateInput(key, isValid, errrorMessage) {
    return {
        type: VALIDATE_INPUT,
        payload: {
            key,
            isValid,
            errrorMessage
        }
    };
}

//Valuation widget
export function tabChange(tab) {
    return {
        type: TAB_CHANGE,
        payload: tab
    };
}

export function thresholdChange(isLess) {
    return {
        type: THRESHOLD_CHANGE,
        payload: isLess
    };
}

export function privateConditionChange(condition) {
    return {
        type: PRIVATE_CONDITION_CHANGE,
        payload: condition
    };
}

export function dealerConditionChange(condition) {
    return {
        type: DEALER_CONDITION_CHANGE,
        payload: condition
    };
}

export function tradeInConditionChange(condition) {
    return {
        type: TRADE_IN_CONDITION_CHANGE,
        payload: condition
    };
}

export function explainerModalShow(bool) {
    return {
        type: EXPLAINER_MODAL_SHOW,
        payload: bool
    };
}

export function explainerConditionChange(condition) {
    return {
        type: EXPLAINER_CONDITION_CHANGE,
        payload: condition
    };
}

export function mileageChangedByUser(bool) {
    return {
        type: MILEAGE_CHANGED_BY_USER,
        payload: bool
    };
}

export function reset() {
    return {
        type: RESET,
        payload: ''
    };
}