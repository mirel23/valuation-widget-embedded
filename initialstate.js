class InitialState {
    constructor() {
        this.valuationWidget = {
            currentStep: 1,
            activeTab: 'Buying',            // dealer, private, trade
            privateCondition: "avg",        // low, avg, high
            dealerCondition: "avg",         // low, avg, high
            tradeInCondition: "average",    // poor, average, excellent
            showExplainerModal: false,
            explainerCondition: "average",  // poor, average, excellent,
            lessThanThreshold: false,
            mileageChangedByUser: false
        };

        this.valuationForm = {
            currentStep: 0,
            wrongPlate: undefined,
            disableNext: false,
            isMobile: false,
            partener: '',
            updateMileage: false,
            collectedDetails: {
                vrm: {
                    value: '',
                    errrorMessage: '',
                    isValid: undefined,
                    isTouched: false,
                    label: 'VRM'
                },
                mileage: {
                    value: '',
                    label: 'Mileage'
                },
                intention: {
                    value: '',
                    label: 'Intention'
                },
                name: {
                    value: '',
                    errrorMessage: '',
                    isValid: undefined,
                    isTouched: false,
                    label: 'Name'
                },
                email: {
                    value: '',
                    errrorMessage: '',
                    isValid: undefined,
                    isTouched: false,
                    label: 'Email'
                },
                postcode: {
                    value: '',
                    errrorMessage: '',
                    isValid: undefined,
                    isTouched: false,
                    label: 'Postcode'
                },
                phone: {
                    value: '',
                    errrorMessage: '',
                    isValid: undefined,
                    isTouched: false,
                    label: 'Phone'
                }
            },
            vehicleDetails: {
                brand: "",
                carImageUrl: "",
                colour: "",
                hasNoInformation: false,
                isSupportedForTCO: false,
                isupportedForValuation: false,
                manufacturer: "",
                model: "",
                type: "",
                vrm: "",
                year: "",
                yearMonth: "",
                yearManufacture: "",
                mileageFromMot: undefined,
                error: undefined
            }
        };
    }
}

export default InitialState;
