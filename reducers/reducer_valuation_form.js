import { fromJS } from 'immutable';

import InitialState from '../initialstate';

import {
    STEP_CHANGE,
    UPDATE_MILEAGE_CHANGE,
    COLLECTED_DETAILS_CHANGE,
    VEHICLE_DETAILS_CHANGE,
    VALIDATE_INPUT,
    WRONG_PLATE_CHANGE,
    DISABLE_NEXT_STEP,
    TOUCH_INPUT,
    IS_MOBILE_CHANGE,
    PARTENER_CHANGE,
    RESET
} from '../actions/types';

let initialState = new InitialState();
const initialStateImmutable = fromJS(initialState.valuationForm);

module.exports = function(state = initialStateImmutable, action){
    switch (action.type) {
        case STEP_CHANGE:
            return state.set('currentStep', action.payload);
        case IS_MOBILE_CHANGE:
            return state.set('isMobile', action.payload);
        case UPDATE_MILEAGE_CHANGE:
            return state.set('updateMileage', action.payload);
        case PARTENER_CHANGE:
            return state.set('partener', action.payload);
        case WRONG_PLATE_CHANGE:
            return state.set('wrongPlate', action.payload);
        case DISABLE_NEXT_STEP:
            return state.set('disableNext', action.payload);
        case COLLECTED_DETAILS_CHANGE:
            return state.setIn(['collectedDetails', action.payload.key, 'value'], action.payload.value);
        case VEHICLE_DETAILS_CHANGE:
            return state.setIn(['vehicleDetails', 'brand'], action.payload.brand)
                .setIn(['vehicleDetails', 'carImageUrl'], action.payload.carImageUrl)
                .setIn(['vehicleDetails', 'colour'], action.payload.colour)
                .setIn(['vehicleDetails', 'hasNoInformation'], action.payload.hasNoInformation)
                .setIn(['vehicleDetails', 'isSupportedForTCO'], action.payload.isSupportedForTCO)
                .setIn(['vehicleDetails', 'isupportedForValuation'], action.payload.isupportedForValuation)
                .setIn(['vehicleDetails', 'manufacturer'], action.payload.manufacturer)
                .setIn(['vehicleDetails', 'mileageFromMot'], action.payload.mileageFromMot)
                .setIn(['vehicleDetails', 'model'], action.payload.model)
                .setIn(['vehicleDetails', 'type'], action.payload.type)
                .setIn(['vehicleDetails', 'vrm'], action.payload.vrm)
                .setIn(['vehicleDetails', 'year'], action.payload.year)
                .setIn(['vehicleDetails', 'yearMonth'], action.payload.yearMonth)
                .setIn(['vehicleDetails', 'yearManufacture'], action.payload.yearManufacture)
                .setIn(['vehicleDetails', 'error'], action.payload.error);
        case TOUCH_INPUT:
            return state.setIn(['collectedDetails', action.payload, 'isTouched'], true);
        case VALIDATE_INPUT:
            return state.setIn(['collectedDetails', action.payload.key, 'isValid'], action.payload.isValid)
                .setIn(['collectedDetails', action.payload.key, 'errrorMessage'], action.payload.errrorMessage);
        case RESET:
            return initialState.valuationForm;
        default: return state;
    }
};