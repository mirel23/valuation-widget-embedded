import { combineReducers } from 'redux';

import valuationWidgetReducer from './reducer_valuation_widget';
import valuationFormReducer from './reducer_valuation_form';

const rootReducer = combineReducers({
    valuationWidget: valuationWidgetReducer,
    valuationForm: valuationFormReducer
});

export default rootReducer;