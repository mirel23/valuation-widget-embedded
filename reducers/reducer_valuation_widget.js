import { fromJS } from 'immutable';

import InitialState from '../initialstate';

import {
    RESET,
    TAB_CHANGE,
    PRIVATE_CONDITION_CHANGE,
    DEALER_CONDITION_CHANGE,
    TRADE_IN_CONDITION_CHANGE,
    EXPLAINER_MODAL_SHOW,
    EXPLAINER_CONDITION_CHANGE,
    MILEAGE_CHANGED_BY_USER,
    THRESHOLD_CHANGE
} from '../actions/types';

let initialState = new InitialState();
const initialStateImmutable = fromJS(initialState.valuationWidget);

module.exports = function(state = initialStateImmutable, action){
    switch (action.type) {
        case TAB_CHANGE:
            return state.set('activeTab', action.payload);
        case THRESHOLD_CHANGE:
            return state.set('lessThanThreshold', action.payload);
        case PRIVATE_CONDITION_CHANGE:
            return state.set('privateCondition', action.payload);
        case DEALER_CONDITION_CHANGE:
            return state.set('dealerCondition', action.payload);
        case TRADE_IN_CONDITION_CHANGE:
            return state.set('tradeInCondition', action.payload);
        case EXPLAINER_MODAL_SHOW:
            return state.set('showExplainerModal', action.payload);
        case EXPLAINER_CONDITION_CHANGE:
            return state.set('explainerCondition', action.payload);
        case MILEAGE_CHANGED_BY_USER:
            return state.set('mileageChangedByUser', action.payload);
        case RESET:
            return initialState.valuationWidget;
        default: return state;
    }
}