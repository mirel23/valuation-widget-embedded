import React, { Component } from "react";
import ReactDom from "react-dom";
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxPromise from 'redux-promise'
import { checkNested } from '../../js/modules/Helpers';

import ValuationWidgetWrapper from './components/ValuationWidgetWrapper';
import reducers from "./reducers";

const createStoreWithMiddleware = applyMiddleware(ReduxPromise)(createStore);
const app = $('#new-valuation-widget-react-embedded');

class App extends Component {
    render() {
        return (
            <Provider store={createStoreWithMiddleware(reducers)}>
                <ValuationWidgetWrapper partener={app.data('partener')}/>
            </Provider>
        );
    }
}

if (app.length && app.data('partener')) {
    ReactDom.render(<App />, app[0]);
}
