import React from 'react';
import classNames from 'classnames';

// Components

// Consts
const hpitracking = HPI.tracking;
const CRUDService = HPI.CRUDService;
const toasts      = HPI.toast;


class PrevNextControls extends React.Component {

    render () {

        let nextBtnClass = classNames({
            'btn--disabled-border': this.props.disableNext,
            'widget-react-embedded__step__controls__nav-button widget-react-embedded__step__controls__nav-button--next': true
        })

        return (
            <div className="widget-react-embedded__step__controls">
                {this.props.hasPrev === 'yes' &&
                <button className="widget-react-embedded__step__controls__nav-button widget-react-embedded__step__controls__nav-button--prev"
                        onClick={(e) => {
                            e.preventDefault();
                            this.props.handleControl('prev');
                        }}></button>
                }

                {this.props.hasNext === 'yes' &&
                <button className={nextBtnClass}
                        onClick={(e) => {
                            e.preventDefault();
                            this.props.handleControl('next');
                        }}>Next Step</button>
                }
            </div>
        )
    }
}

export default PrevNextControls;
