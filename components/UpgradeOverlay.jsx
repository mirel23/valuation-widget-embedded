import React from 'react';

const Upgrade = (props) => {

    return(
        <div className="overlay-upgrade text-center">
            <div className="wrap-button">
                <p>Upgrade your valuation now to get</p>
                <ul>
                    { props.availableData.tradeIn && <li><strong>Trade-in values</strong> - based on the same valuations dealers themselves use</li> }
                    { props.availableData.dealership && props.availableData.private && <li><strong>Dealership estimates</strong> - impartial and accurate using data from  millions sales</li> }
                    { props.availableData.hasTimeline && <li><strong>Depreciation timeline</strong> - understand how the value has changed (where available)</li> }
                    <li><strong>Tax and Running Costs</strong> - to give you an idea of what it'll really cost to own the vehicle</li>
                </ul>
                <a className="button secondary-button radius round input--white" href={props.upgradelink}><span>Upgrade for £{HPIDATA.globals.product.price}</span></a>
            </div>
        </div>
    );
};

export default Upgrade;