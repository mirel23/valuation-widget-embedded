import React from 'react';
import CommaChameloen from '../../../js/modules/CommaChameleon';
import Modal from './Modal';
import MilesFormEmbedded from '../containers/MilesFormEmbedded';

class EstimateMileage extends React.Component {
    state = {
        isOpen: false
    }

    toggleModal = () => {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {
            return (
                <div className="estimate-mileage">
                    <Modal show={this.state.isOpen} onClose={this.toggleModal} options={{
                        size: 'tiny',
                        position: 'center'
                    }}>
                        <MilesFormEmbedded mileage={this.props.mileage} onClose={this.toggleModal}/>
                    </Modal>

                    <span className="estimate-mileage__text make-text-align-left">Mileage</span>
                    <span className="estimate-mileage__mileage make-text-align-left">{CommaChameloen.commaString(this.props.mileage)} { HPIDATA.canUpdateMileage && <span className="estimate-mileage__change-link make-text-align-center" onClick={() => this.toggleModal()}>Change</span> }</span>

                    {!this.props.mileageChangedByUser && <span className="estimate-mileage__text make-text-align-left">(estimated)</span>}
                </div>
            )
        }
}

export default EstimateMileage;
