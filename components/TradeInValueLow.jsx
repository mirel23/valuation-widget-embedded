import React from 'react';
import classNames from 'classnames';

class ValueLow extends React.Component {
    render () {

        let wrapperClass = classNames({
            'value-wrapper value-wrapper-low': true,
            'threshold-msg': this.props.lessThenThreshold
        });

        return (
            <div className={wrapperClass}>
                <div className="value">
                <span className="value-content">
                    <span>£{this.props.shownValue} <small>or less</small></span>
                </span>
                </div>

                <p>Offers from dealers on cars like this will vary significantly based on the vehicle being purchased. This car
                    would normally be worth <em>£{this.props.val.low} - £{this.props.val.high}</em> at auction.</p>
            </div>
        )
    }
}

export default ValueLow;
