import React from 'react';
import classNames from 'classnames';
const ValidationHelpers = HPI.ValidationHelpers;
import emailValidator from '../../../js/modules/emailValidator';
const CRUDService = HPI.CRUDService;

class FormFeedback extends React.Component {
    state = {
        toggle: false,
        email: window.HPIDATA.report.collectedDetails.email,
        telephone: window.HPIDATA.report.collectedDetails.telephone,
        buttonState: 'This is correct',
        wasSubmitted: false,
        emailTouched: false,
        telephoneTouched: false,
        emailError: false,
        telephoneError: false
    };

    showError = (inputName) => {
        return this.state.wasSubmitted || this.state[inputName];
    }

    handleInput = (e, type, action) => {

        if (action === 'blur') {
            if (type === 'email') {
                this.setState({
                    emailTouched: true,
                    emailError: !emailValidator.checkAddress(e.target.value)
                });
            } else {
                this.setState({
                    telephoneTouched: true,
                    telephoneError: !ValidationHelpers.validatePhone(e.target.value)
                });
            }
        } else if (action === 'change') {
            if (type === 'email') {
                this.setState({
                    email: e.target.value,
                    buttonState: 'Save'
                });

                if (this.showError('emailTouched')) {
                    this.setState({ emailError: !emailValidator.checkAddress(e.target.value) });
                }
            } else {
                this.setState({
                    telephone: e.target.value,
                    buttonState: 'Save'
                });

                if (this.showError('telephoneTouched')) {
                    this.setState({ telephoneError: !ValidationHelpers.validatePhone(e.target.value) });
                }
            }
        }
    }


    preventPropagation = (e) => {
        e.stopPropagation();
    }

    close = () => {
        this.props.close();
    }

    handleSubmit = (e) => {
        e.preventDefault()

        this.setState({
            wasSubmitted : true,
            emailError: !emailValidator.checkAddress(this.state.email),
            telephoneError: !ValidationHelpers.validatePhone(this.state.telephone)
        });

        if(emailValidator.checkAddress(this.state.email) && ValidationHelpers.validatePhone(this.state.telephone)) {

            CRUDService.post("/api/leads/update-user-details", {
                reference: window.HPIDATA.report.reference,
                email: this.state.email,
                phone: this.state.telephone
            });

            this.props.handleClose();
        }
    };

    render() {
        let emailClass = classNames({
            'email-input': true,
            'has-error': this.state.emailError
        });

        let phoneClass = classNames({
            'telephone-input': true,
            'has-error': this.state.telephoneError
        });

        return (
            <div>
                <form className="form-modal"  onSubmit={(e) => this.handleSubmit(e)} onClick={() => this.close()} >
                    <div className="modal-content" onClick={(event) => this.preventPropagation(event)}>
                        <p className="input-title">Please confirm your contact details</p>

                        <p className="input-paragraph">Email</p>
                        <input className={emailClass} type="text" placeholder="Email" value={this.state.email} onChange={(e) => this.handleInput(e, 'email', 'change')}  onBlur={(e) => this.handleInput(e, 'email', 'blur')}/>
                        {this.state.emailError && <div className="error-message error-message--invalid"><p>Please enter a valid email</p></div> }


                        <p className="input-paragraph">Phone</p>
                        <input className={phoneClass} type="text" placeholder="Telephone" value={this.state.telephone} onChange={(e) => this.handleInput(e, 'phone', 'change')} onBlur={(e) => this.handleInput(e, 'phone', 'blur')}/>
                        {this.state.telephoneError && <div className="error-message error-message--invalid"><p>Please enter a valid phone number</p></div>}

                        <button type="submit">{this.state.buttonState}</button>
                    </div>
                </form>
            </div>
        );
    }
}

export default FormFeedback;
