import React from 'react';
import classNames from 'classnames';

class Modal extends React.Component {
    preventPropagation = (e) => {
        e.stopPropagation();
    }

    close = () => {
        this.props.onClose();
    }

    render() {
        if(!this.props.show) {
            return null;
        }

        let header = <div className="react-modal__header">
            <h2>{this.props.options.header}</h2>
        </div>;

        let closeBtn = <div className="react-modal__close">&times;</div>;

        let contentClassName = classNames({
            'react-modal__wrapper': true,
            ['react-modal__wrapper--' + this.props.options.size]: true,
            ['react-modal__wrapper--' + this.props.options.position]: true,
        });

        return (
            <div>
                <div className="react-modal" onClick={() => this.close()} >
                    <div className={contentClassName} onClick={(event) => this.preventPropagation(event)}>
                        {this.props.options.hasCloseBtn && closeBtn}
                        {this.props.options.header && header}
                        <div className="react-modal__content">{this.props.children}</div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Modal;
