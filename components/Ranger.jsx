import React from 'react';
import _ from 'lodash';


class Ranger extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            dotStyle: {}
        }

        this.isDragging = false;
        this.dragStart = this.dragStart.bind(this);
        this.dragEnd = this.dragEnd.bind(this);
        this.dragging = this.dragging.bind(this);

        this.mouseDown = this.mouseDown.bind(this);
        this.mouseUp = this.mouseUp.bind(this);
        this.mouseMove = this.mouseMove.bind(this);
    }

    componentDidMount() {
        this.rangerBarWidth = ReactDOM.findDOMNode(this.rangerBar).getBoundingClientRect().width;
    }

    dragStart(e) {
        e.stopPropagation();
        this.isDragging = true;
        this.dragStartX = e.changedTouches ? e.changedTouches[0].screenX : e.pageX;
    }

    dragEnd(e) {
        this.isDragging = false;
        let currentCondition = this.props.currentCondition;
        let condArr = ["poor", "average", "excellent"];
        let currentIndex = condArr.indexOf(currentCondition);
        let dragEndX = e.changedTouches ? e.changedTouches[0].screenX : e.pageX;
        let dragLength = dragEndX - this.dragStartX;
        let nextIndex;
        let tabThird = this.rangerBarWidth / 3 * 2;
        let tabHalf = this.rangerBarWidth / 3;

        switch(true) {
        case (dragLength < -tabThird):
            nextIndex = currentIndex - 2 < 0 ? 0 : currentIndex - 2;
            break;
        case (dragLength < -tabHalf):
            nextIndex = currentIndex - 1 < 0 ? 0 : currentIndex - 1;
            break;
        case (dragLength > tabThird):
            nextIndex = currentIndex + 2 >= condArr.length ? condArr.length - 1 : currentIndex + 2;
            break;
        case (dragLength > tabHalf):
            nextIndex = currentIndex + 1 >= condArr.length ? condArr.length - 1 : currentIndex + 1;
            break;
        default:
            nextIndex = currentIndex;
            // code block
        }

        this.props.onConditionChange(condArr[nextIndex], "report-ranger-label");

        this.setState({
            dotStyle: {}
        });
    }

    dragging(e) {
        let dragEndX = e.changedTouches ? e.changedTouches[0].screenX : e.pageX;
        let dragLength = dragEndX - this.dragStartX;
        let dragDistance = 0;

        if(this.props.currentCondition === 'average') {
            dragDistance = this.rangerBarWidth / 2 + dragLength;
        }

        if(this.props.currentCondition === 'excellent') {
            dragDistance = this.rangerBarWidth + dragLength;
        }

        if(this.props.currentCondition === 'poor') {
            dragDistance = dragLength;
        }

        if(dragDistance > this.rangerBarWidth) {
            dragDistance = this.rangerBarWidth;
        }

        if(dragDistance < 0) {
            dragDistance = 0;
        }

        let dotStyle = {
            'left': `${dragDistance}px`,
            'boxShadow': '0px 0px 10px white'
        }

        this.setState({
            dotStyle: dotStyle
        });
    }

    mouseDown(e) {
        this.dragStart(e);
        window.addEventListener("mousemove", this.mouseMove);
        window.addEventListener("mouseup", this.mouseUp);
    }

    mouseUp(e) {
        if(this.isDragging) {
            this.dragEnd(e);
        }

        window.removeEventListener("mousemove", this.mouseMove);
        window.removeEventListener("mouseup", this.mouseUp);
    }

    mouseMove(e) {
        this.dragging(e);
    }

    render() {
        return (
          <div className="report-ranger" onMouseLeave={this.mouseUp}>

              <a href="#" className="report-ranger-info" onClick={ () => this.props.helpModal(true) }>
                  <span>Info</span>
                  <i className="icon-question-circle"></i>
              </a>

              <div className="report-ranger-title">Condition</div>

              <div className="report-ranger-interactive">

                  <div className={this.props.rangerClass}>
                      <div className="ranger-handle"
                          onMouseDown={this.mouseDown}
                          onTouchStart={this.dragStart}
                          onTouchMove={this.dragging}
                          onTouchEnd={this.dragEnd}
                          style={this.state.dotStyle}
                          >
                      </div>
                  </div>

                  <div className="report-ranger-label-wrapper">
                      <a className={this.props.box1Class} onClick={ () => this.props.onConditionChange("poor", "report-ranger-label")} >Poor</a>
                      <a className={this.props.box2Class} onClick={ () => this.props.onConditionChange("average", "report-ranger-label") }>Good</a>
                      <a className={this.props.box3Class} onClick={ () => this.props.onConditionChange("excellent", "report-ranger-label") }>Excellent</a>
                  </div>

                  <div className="ranger">
                      <div className="ranger-bar" ref={(rangerBar) => { this.rangerBar = rangerBar; }}></div>
                  </div>

              </div>
          </div>
        );
    }
}

export default Ranger;
