import React from 'react';

class Feedback extends React.Component {
    state = {
        message: ""
    }

    preventPropagation = (e) => {
        e.stopPropagation();
    }

    close = (source) => {
        if (source === "outside" && this.state.message) {
            return;
        } else {
            this.props.close();
        }
    }

    redirectToFacebook = () => {
        this.props.close();
        window.open(HPIDATA.globals.facebookReviewsURL, '_blank');
    }

    render() {
        let text = this.props.direction === "Up" ? "liked it." : "didn't like it.";

        let thumbDownTemplate = <div>
            <p>Thank you for your feedback! If you can, please tell us more about why you {text}</p>
            <form onSubmit={(event) => this.props.submit(event, this.state.message)}>
                <textarea rows="4" onChange={(event) => this.setState({message: event.target.value})}></textarea>
                <div className="actions">
                    <button className="close-btn" onClick={() => this.close("btn")}>CLOSE</button>
                    <button type="submit" className="send-btn">SEND</button>
                </div>
            </form>
        </div>;

        let thumbUpTemplate = <div>
            <p>Thank you for the feedback! If you appreciated the service, please leave us a review on our facebook page.</p>
            <div className="actions">
                <button className="close-btn" onClick={() => this.close("btn")}>MAYBE LATER</button>
                <button className="send-btn" onClick={() => this.redirectToFacebook()}>LEAVE REVIEW</button>
            </div>
        </div>;

        return (
            <div className="feedback" onClick={() => this.close("outside")}>
                <div className="feedback-form" onClick={(event) => this.preventPropagation(event)}>
                    {this.props.direction === "Up" ? thumbUpTemplate : thumbDownTemplate}
                </div>
            </div>
        );
    }
}

export default Feedback;
