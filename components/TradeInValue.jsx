import React from 'react';

const ValueDefault = ({val}) => {
    return (
        <div className="value-wrapper">
            <div className="value">
                <span className="value-content">
                    <span>£{val.low}</span>
                    <span className="desc">Low</span>
                </span>
                <span className="seperator"></span>
                <span className="value-content">
                    <span>£{val.high}</span>
                    <span className="desc">High</span>
                </span>
            </div>
        </div>
    )
}

export default ValueDefault;
