/* eslint-disable */
import React from 'react';

import ValuationForm from '../containers/ValuationForm';

class ValuationWidgetWrapper extends React.Component {
    render() {
        return (
            <ValuationForm partener={this.props.partener}/>
        );
    }
}

export default ValuationWidgetWrapper;
