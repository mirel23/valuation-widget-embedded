import React from 'react';

class Thumbs extends React.Component {
    render() {
        return (
            <div className="thumbs-container">
                <h6>Rate this</h6>
                <div className="thumbs">
                    <div className="thumbs-up" onClick={() => this.props.rate('Up')}>
                        <i className="icon-thumbs-up"></i>
                    </div>
                    <div className="thumbs-down" onClick={() => this.props.rate('Down')}>
                        <i className="icon-thumbs-down"></i>
                    </div>
                </div>
            </div>
        );
    }
}

export default Thumbs;
