import React from 'react';

// Components

// Consts
const hpitracking = HPI.tracking;
const CRUDService = HPI.CRUDService;
const toasts      = HPI.toast;


class VehicleInformation extends React.Component {

    get vehicleImage() {
        let url = this.props.vehicleDetails && this.props.vehicleDetails.get('carImageUrl') ? `https://hpicheck.com${this.props.vehicleDetails.get('carImageUrl')}` : '';
        if (this.props.vehicleDetails.error || this.props.vehicleDetails.hasNoInformation) {
            url = '/assets/img/icons/car.png';
        }
        return url;
    }

    get vehicleMsg() {
        let msg = 'Not your vehicle?';

        if (this.props.vehicleDetails.get('error') || this.props.vehicleDetails.get('hasNoInformation')) {
            msg = this.props.vehicleDetails.get('error') ? 'Please try again' : 'Please try another car';
        }
        return msg;
    }

    get vehicleInfos() {
        let infos;
        if (this.props.vehicleDetails.get('error') || this.props.vehicleDetails.get('hasNoInformation')) {
            infos = 'No vehicle information';
        } else {
            infos = `${this.props.vehicleDetails.get('year')} - ${this.props.vehicleDetails.get('brand')} ${this.props.vehicleDetails.get('model')}`;
        }
        return infos;
    }

    render () {
        return (
            <div className="widget-react-embedded__step__vehicle-info">
                <div className="widget-react-embedded__step__vehicle-info__image">
                    <div className="car-image">
                        <img src={this.vehicleImage} />
                    </div>
                </div>
                <div className="widget-react-embedded__step__vehicle-info__make-and-model">
                   <p>{this.vehicleInfos}</p>
                    <a name="retry" onClick={(e) => {
                        e.preventDefault();
                        this.props.changeVehicle()
                    }}>{this.vehicleMsg}</a>
                </div>
            </div>
        )
    }
}

export default VehicleInformation;
