import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {toObject} from 'immutable';

import Helpers from '../../../js/modules/Helpers';

// Components

// Consts
const hpitracking = HPI.tracking;
const CRUDService = HPI.CRUDService;
const toasts = HPI.toast;

// Redux Actions
import {
    stepChange,
    updateMileageChange,
    mileageChangedByUser
} from '../actions';

class StepSpinner extends React.Component {

    componentDidMount() {
        let item = {
            vrm: this.props.valuationForm.getIn(['collectedDetails', 'vrm', 'value']),
            mileage: this.props.valuationForm.getIn(['collectedDetails', 'mileage', 'value']),
            intention: this.props.valuationForm.getIn(['collectedDetails', 'intention', 'value']),
            name: this.props.valuationForm.getIn(['collectedDetails', 'name', 'value']),
            email: this.props.valuationForm.getIn(['collectedDetails', 'email', 'value']),
            postcode: this.props.valuationForm.getIn(['collectedDetails', 'postcode', 'value']),
            phone: this.props.valuationForm.getIn(['collectedDetails', 'phone', 'value'])
        };

        CRUDService.post(`/i/${this.props.valuationForm.get('partener')}/${this.props.valuationForm.getIn(['vehicleDetails', 'vrm'])}/valuation${this.props.valuationForm.get('updateMileage') ? '?updateMileage=true&leadId=' + HPIDATA.leadId : ''}`, Object.assign({
            vehicle : {
                manufacturer : this.props.valuationForm.getIn(['vehicleDetails', 'manufacturer']),
                yearMonth : this.props.valuationForm.getIn(['vehicleDetails', 'yearMonth']),
                vrm : this.props.valuationForm.getIn(['vehicleDetails', 'vrm'])
            }
        }, item), (valuation) => {
            if(valuation.error){
                this.props.stepChange(0);
                this.props.updateMileageChange(false);

                let collectedDetails = {
                    error: '',
                    collectedEmail: this.props.valuationForm.getIn(['collectedDetails', 'email', 'value']),
                    collectedIntention: this.props.valuationForm.getIn(['collectedDetails', 'intention', 'value']),
                    collectedName: this.props.valuationForm.getIn(['collectedDetails', 'name', 'value']),
                    collectedPostcode: this.props.valuationForm.getIn(['collectedDetails', 'postcode', 'value']),
                    collectedPhone: this.props.valuationForm.getIn(['collectedDetails', 'phone', 'value']),
                    collectedVrm: this.props.valuationForm.getIn(['collectedDetails', 'vrm', 'value']),
                    collectedMileage: this.props.valuationForm.getIn(['collectedDetails', 'mileage', 'value'])
                };

                if(valuation.error === "Sorry: Something went wrong"){
                    toasts.createToast('error', "Sorry we're currently unable to provide a valuation for this vehicle. <a>Click to start again</a>", {screenBlock: true}, () => {
                        this.props.stepChange(0);
                        this.props.updateMileageChange(false);
                    });

                    collectedDetails.error = 'unable to provide a valuation for this vehicle';

                    hpitracking.track('NVF: Show Error: Get Vehicle Valuation', {
                        category: 'New Valuation Form',
                        label: 'Sorry: Something went wrong. VRM ' + this.props.valuationForm.getIn(['collectedDetails', 'vrm', 'value'])
                    }, collectedDetails);
                } else {
                    toasts.createToast('alert', "Sorry, were were unable to generate a valuation", {dismissOnTimeout: false});

                    collectedDetails.error = valuation.error;

                    hpitracking.track('NVF: Show Error: Get Vehicle Valuation', {
                        category: 'New Valuation Form',
                        label: valuation.error
                    }, collectedDetails);
                }
            } else {
                if (!this.props.valuationForm.get('updateMileage')) {
                    Helpers.incrementCookieCount('hv.c', 60);
                }

                Object.assign(HPIDATA, valuation);
                this.props.stepChange(this.props.valuationForm.get('currentStep') + 1);
                if (this.props.valuationForm.get('updateMileage')) {
                    this.props.mileageChangedByUser(true);
                }
                this.props.updateMileageChange(false);
            }
        }), (err) => {
            toasts.createToast('error', err.error, () => {
                this.props.stepChange(0);
                this.props.updateMileageChange(false);
            });
        };
    }

    render() {

        return (
            <div className="widget-react-embedded__step--spinner">
                <div className="widget-react-embedded__wrapper--upper">
                    <div className="row">
                        <div className="small-12 columns make-text-align-center">
                            <img src="/assets/img/35.gif" alt="spinner"/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

// Inject action creators as actions
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        stepChange,
        updateMileageChange,
        mileageChangedByUser
    }, dispatch);
}

// This component will have access to `initialstate().valuationWidget` through `this.props.valuationWidget`
function mapStateToProps(state) {
    return {
        valuationForm: state.valuationForm,
    };
}

// Connects the React component to the Redux store
export default connect(mapStateToProps, mapDispatchToProps)(StepSpinner);
