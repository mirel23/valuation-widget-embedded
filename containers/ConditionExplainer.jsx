/* eslint-disable */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { toObject } from 'immutable';
const hpitracking = HPI.tracking;

// Components
import Conditions from '../components/ConditionsConfig'

// Redux Actions
import {
    tradeInConditionChange,
    explainerModalShow,
    explainerConditionChange
} from '../actions';

class ConditionExplainer extends React.Component {

    state = {
        vehicleYear: HPIDATA.vehicle.year,
        currentYear: new Date().getFullYear()
    }

    shouldComponentUpdate(nextProps, nextState) {
        if(nextProps.valuationWidget.get('explainerCondition') !== this.props.valuationWidget.get('explainerCondition') ||
            nextProps.valuationWidget.get('tradeInCondition') !== this.props.valuationWidget.get('tradeInCondition')){
            return true;
        }
        return false;
    }

    getActiveTabLongName = (type) => {
        let tabs = [{
            activeTab: "dealer",
            nameLong: "Dealership Retail"
        }, {
            activeTab: "private",
            nameLong: "Private Sale"
        }, {
            activeTab: "trade",
            nameLong: "Trade-In"
        }];

        return tabs.filter((r) => {
            return r.activeTab ===  type;
        });
    }

    onConditionChange = (condition) => {
        if (this.props.valuationWidget.get('explainerCondition') === condition) {
            return;
        } else {
            this.props.explainerConditionChange(condition);
        }
    }

    chooseCondition = (condition) => {
        let currentTab = this.getActiveTabLongName(this.props.valuationWidget.get('activeTab'))[0];

        hpitracking.track('RP: Your valuation - condition change', {
            category: 'Report Page',
            label: 'Tab ' + currentTab.nameLong + ': Change from condition ' + this.props.valuationWidget.get('tradeInCondition') + " to condition " + condition + ": Condition changed from 'explainer modal'"
        }, {
            userType: this.props.userType,
            currentTab: currentTab.nameLong,
            currentCondition: this.props.valuationWidget.get('tradeInCondition'),
            nextCondition: condition,
            changeSource: 'explainer modal'
        });

        this.props.tradeInConditionChange(condition);
        this.toggleModal(false);
    }

    toggleModal = (bool) => {
        this.props.explainerModalShow(bool);
    }

    capitalizeFirstLetter = (string) => {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    render() {
        let age = this.state.currentYear - this.state.vehicleYear > 10 ? 10 : this.state.currentYear - this.state.vehicleYear,
            poorConditions, averageConditions, excellentConditions;

        poorConditions = Conditions.conditions[age.toString()].poor.map((result, i) => {
            return (
                <li key={i}><p>{result}</p></li>
            )
        });

        averageConditions = Conditions.conditions[age.toString()].average.map((result, i) => {
            return (
                <li key={i}><p>{result}</p></li>
            )
        });

        excellentConditions = Conditions.conditions[age.toString()].excellent.map((result, i) => {
            return (
                <li key={i}><p>{result}</p></li>
            )
        })

        return (
            <div className="container-foundation">
                <div className="info">
                    <p className="description">Use the following guidelines to help understand and evaluate the condition of the car</p>
                    <div className="explainer-tabs">
                        <div className={`tab ${this.props.valuationWidget.get('explainerCondition') === 'poor' ? 'active' : ''}`}
                             onClick={() => this.onConditionChange("poor")}><p>Poor</p></div>
                        <div className={`tab ${this.props.valuationWidget.get('explainerCondition') === 'average' ? 'active' : ''}`}
                             onClick={() => this.onConditionChange("average")}><p>Good</p></div>
                        <div className={`tab ${this.props.valuationWidget.get('explainerCondition') === 'excellent' ? 'active' : ''}`}
                             onClick={() => this.onConditionChange("excellent")}><p>Excellent</p></div>
                    </div>
                </div>
                <div className="actions" onClick={ () => this.toggleModal(false) }>
                    <button><i className="icon-close"></i></button>
                </div>
                <div className="explainer-content">
                    <div className={ 'sections ' + this.props.valuationWidget.get('explainerCondition') }>
                        <div className="section">
                            <div className="section-content">
                                <div className="text">
                                    <ul>{poorConditions}</ul>
                                </div>
                            </div>
                        </div>
                        <div className="section">
                            <div className="section-content">
                                <div className="text">
                                    <ul>{averageConditions}</ul>
                                </div>
                            </div>
                        </div>
                        <div className="section">
                            <div className="section-content">
                                <div className="text">
                                    <ul>{excellentConditions}</ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='explainer-button'>
                    <div className="choose-button">
                        <button onClick={ () => this.chooseCondition(this.props.valuationWidget.get('explainerCondition')) }>Choose { this.capitalizeFirstLetter(this.props.valuationWidget.get('explainerCondition')) }</button>
                    </div>
                </div>
            </div>
        )
    }
}

// Inject action creators as actions
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        tradeInConditionChange,
        explainerModalShow,
        explainerConditionChange
    }, dispatch);
}

// This component will have access to `initialstate().valuationWidget` through `this.props.valuationWidget`
function mapStateToProps(state) {
    return {
        valuationWidget: state.valuationWidget,
    };
}

// Connects the React component to the Redux store
export default connect(mapStateToProps, mapDispatchToProps)(ConditionExplainer);
