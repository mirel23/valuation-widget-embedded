/* eslint-disable */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { toObject } from 'immutable';
import classNames from 'classnames';
import CommaChameloen from '../../../js/modules/CommaChameleon';
import TradeInValueLow from '../components/TradeInValueLow';
import TradeInValue from '../components/TradeInValue';
import Ranger from '../components/Ranger';
const hpitracking = HPI.tracking;

// Redux Actions
import {
    tradeInConditionChange,
    explainerModalShow,
    explainerConditionChange,
    thresholdChange
} from '../actions';


class ContentTradeIn extends React.Component {

    constructor(props) {
        super(props);

        this.tradeInThreshold = {
            min: 500,
            max: 600
        };
    }

    getActiveTabLongName = (type) => {
        let tabs = [{
            activeTab: "dealer",
            nameLong: "Dealership Retail"
        }, {
            activeTab: "private",
            nameLong: "Private Sale"
        }, {
            activeTab: "trade",
            nameLong: "Trade-In"
        }];

        return tabs.filter((r) => {
            return r.activeTab ===  type;
        });
    }

    showValue = (type) => {
        return this.props.showUpgrade ? "??,???" : CommaChameloen.commaString(this.props.valuation.tradein[this.props.valuationWidget.get('tradeInCondition')][type]);
    }

    onConditionChange = (condition, clickedFrom) => {
        if (this.props.valuationWidget.get('tradeInCondition') === condition) {
            return;
        } else {
            let currentTab = this.getActiveTabLongName(this.props.valuationWidget.get('activeTab'))[0];

            hpitracking.track('RP: Your valuation - condition change', {
                category: 'Report Page',
                label: 'Tab ' + currentTab.nameLong + ': Change from condition ' + this.props.valuationWidget.get('tradeInCondition') + " to condition " + condition + ": Condition changed from '" + clickedFrom + "'"
            }, {
                userType: this.props.userType,
                currentTab: currentTab.nameLong,
                currentCondition: this.props.valuationWidget.get('tradeInCondition'),
                nextCondition: condition,
                changeSource: clickedFrom
            });

            this.props.tradeInConditionChange(condition);
        }
    }

    toggleModal = (bool) => {
        let currentTab = this.getActiveTabLongName(this.props.valuationWidget.get('activeTab'))[0];

        hpitracking.track('RP: Your valuation - condition explainer modal opened', {
            category: 'Report Page',
            label: 'Tab ' + currentTab.nameLong + ': Opend from condition ' + this.props.valuationWidget.get('tradeInCondition')
        }, {
            userType: this.props.userType,
            currentTab: currentTab.nameLong,
            currentCondition: this.props.valuationWidget.get('tradeInCondition')
        });

        this.props.explainerConditionChange(this.props.valuationWidget.get('tradeInCondition'));
        this.props.explainerModalShow(bool);
    }


    getCondition() {
        return this.props.valuationWidget.get('tradeInCondition');
    }

    getConditionHighValue() {
        return this.props.valuation.tradein[this.getCondition()].high;
    }

    _templateValue() {
        let val = {
            low : this.showValue("low") ? this.showValue("low") : 0,
            high : this.showValue("high")
        };

        let condition = this.getConditionHighValue() < this.tradeInThreshold.max;

        let shownValue;

        if (this.getConditionHighValue() < this.tradeInThreshold.min) {
            shownValue = 600;
        } else if (this.getConditionHighValue() >= this.tradeInThreshold.min && this.getConditionHighValue() < this.tradeInThreshold.max) {
            shownValue = 700;
        }

        this.props.thresholdChange(val.high && condition);

        return (
            <div>
                { val.high && condition ?
                    <TradeInValueLow val={val} shownValue={shownValue} lessThenThreshold={this.props.valuationWidget.get('lessThanThreshold')}/> :
                    <TradeInValue val={val} /> }
            </div>
        )
    }

    render() {

        let rangerClass = classNames({
            "ranger": true,
            "ranger-0": this.props.valuationWidget.get('tradeInCondition') === "poor" ? true : false,
            "ranger-50": this.props.valuationWidget.get('tradeInCondition') === "average" ? true : false,
            "ranger-100": this.props.valuationWidget.get('tradeInCondition') === "excellent" ? true : false
        });

        let box1Class = classNames({
            "report-ranger-label": true,
            "active": this.props.valuationWidget.get('tradeInCondition') === "poor" ? true : false
        });

        let box2Class = classNames({
            "report-ranger-label": true,
            "active": this.props.valuationWidget.get('tradeInCondition') === "average" ? true : false
        });

        let box3Class = classNames({
            "report-ranger-label": true,
            "active": this.props.valuationWidget.get('tradeInCondition') === "excellent" ? true : false
        });

        return (
            <div className="trade-in-tab">
                {this._templateValue()}

                <div className="logo">
                    <img src="/assets/img/logos/dark_logo_valuation_with_stamp.png" alt="hpivaluation logo" />
                </div>

                <Ranger
                    rangerClass={rangerClass}
                    box1Class={box1Class}
                    box2Class={box2Class}
                    box3Class={box3Class}
                    onConditionChange={this.onConditionChange}
                    helpModal={this.toggleModal}
                    currentCondition={this.props.valuationWidget.get('tradeInCondition')}
                />
            </div>
        )
    }
}

// Inject action creators as actions
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        tradeInConditionChange,
        explainerModalShow,
        explainerConditionChange,
        thresholdChange
    }, dispatch);
}

// This component will have access to `initialstate().valuationWidget` through `this.props.valuationWidget`
function mapStateToProps(state) {
    return {
        valuationWidget: state.valuationWidget,
    };
}

// Connects the React component to the Redux store
export default connect(mapStateToProps, mapDispatchToProps)(ContentTradeIn);
