import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { toObject } from 'immutable';
import classNames from 'classnames';

// Redux Actions
import { stepChange,
    collectedDetailsChange
} from '../actions';

// Components
import PrevNextControls from '../components/PrevNextControls';

// Consts
const hpitracking = HPI.tracking;
const CRUDService = HPI.CRUDService;
const toasts      = HPI.toast;


class StepIntention extends React.Component {

    nextStep = (e, intention) => {
        e.preventDefault();
        this.props.collectedDetailsChange('intention', intention);
        this.props.stepChange(this.props.valuationForm.get('currentStep') + 1);
    }

    handleControl = (direction) => {
        if (direction === 'next') {
            this.nextStep();
        } else {
            this.prevStep();
        }
    }

    prevStep = () => {
        this.props.stepChange(this.props.valuationForm.get('currentStep') - 1);
    }

    render () {

        let btnClass = (intention) => {
            return classNames({
                'button radius round button--white input-lg': true,
                'button--white__active': this.props.valuationForm.getIn(['collectedDetails', 'intention', 'value']) === intention
            });
        }

        return (
            <div className="widget-react-embedded__step--intention">
                <div className="widget-react-embedded__wrapper--upper">
                    <div className="row">
                        <div className="small-12 columns make-text-align-center">
                            <p className="title">Are you...</p>

                            <div className="inputs-form">
                                <div className="input-group">
                                    <a className={btnClass('Selling')} name="Selling" href="" onClick={(e) => this.nextStep(e, 'Selling')}>
                                        Selling a car <i className="icon-check-circle"></i><i className="icon-question-circle"></i>
                                    </a>

                                    <a className={btnClass('Buying')} name="Buying" href="" onClick={(e) => this.nextStep(e, 'Buying')}>
                                        Buying a car <i className="icon-check-circle"></i><i className="icon-question-circle"></i>
                                    </a>
                                    <a className={btnClass('Curious')} name="Curious" href="" onClick={(e) => this.nextStep(e, 'Curious')}>
                                        Just curious <i className="icon-check-circle"></i><i className="icon-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="widget-react-embedded__wrapper--lower">
                    <div className="row">
                        <div className="small-12 columns make-text-align-center">
                            <PrevNextControls handleControl={this.handleControl} hasNext="no" hasPrev="yes"/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

// Inject action creators as actions
function mapDispatchToProps (dispatch) {
    return bindActionCreators({
        stepChange,
        collectedDetailsChange
    }, dispatch);
}

// This component will have access to `initialstate().valuationWidget` through `this.props.valuationWidget`
function mapStateToProps (state) {
    return {
        valuationForm: state.valuationForm,
    };
}

// Connects the React component to the Redux store
export default connect(mapStateToProps, mapDispatchToProps)(StepIntention);
