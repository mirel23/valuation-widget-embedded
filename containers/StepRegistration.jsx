import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { toObject } from 'immutable';
import classNames from 'classnames';

import Helpers from '../../../js/modules/Helpers';
// Redux Actions
import {
    collectedDetailsChange,
    disableNextStep,
    stepChange,
    touchInput,
    validateInput,
    vehicleDetailsChange,
    wrongPlateChange
} from '../actions';

// Components

// Consts
const hpitracking = HPI.tracking;
const CRUDService = HPI.CRUDService;
const ValidationHelpers = HPI.ValidationHelpers;
const toasts = HPI.toast;
const MAX_VALUATIONS = 8;


class StepRegistration extends React.Component {

    constructor (props) {
        super(props);
        this.handleInputChange = _.debounce(this.handleInputChange.bind(this), 500);
    }

    componentDidMount() {
        let collectedDetails = {
            collectedEmail: this.props.valuationForm.getIn(['collectedDetails', 'email', 'value']),
            collectedIntention: this.props.valuationForm.getIn(['collectedDetails', 'intention', 'value']),
            collectedName: this.props.valuationForm.getIn(['collectedDetails', 'name', 'value']),
            collectedPostcode: this.props.valuationForm.getIn(['collectedDetails', 'postcode', 'value']),
            collectedPhone: this.props.valuationForm.getIn(['collectedDetails', 'phone', 'value']),
            collectedVrm: this.props.valuationForm.getIn(['collectedDetails', 'vrm', 'value']),
            collectedMileage: this.props.valuationForm.getIn(['collectedDetails', 'mileage', 'value'])
        };

        if (this.showNotificationBox()) {
            hpitracking.track('NVF: Show Step: Valuation count exceeded', {
                category: 'New Valuation Form',
                label: 'Show notification'
            }, collectedDetails);
        } else {
            this.vrmInput.focus();
        }
    }

    componentDidUpdate(prevProps, prevState) {
        let disableNext = (this.props.valuationForm.getIn(['collectedDetails', 'vrm', 'isValid']) === false
            && this.props.valuationForm.getIn(['collectedDetails', 'vrm', 'isTouched']));

        this.props.disableNextStep(disableNext);
    }

    showNotificationBox = () => {
        return Helpers.getCookie('hv.c') > MAX_VALUATIONS;
    }

    validateStep = () => {
        return this.props.valuationForm.getIn(['collectedDetails', 'vrm', 'isValid']) === true && !this.props.valuationForm.get('wrongPlate');
    }

    handleInput = (value, inputName, eventName) => {
        this.props.collectedDetailsChange(inputName, value);

        switch (inputName) {
        case 'vrm':
            let checkedVRM = ValidationHelpers.validateAndFixVRM(value);
            this.props.validateInput('vrm', !!checkedVRM, !!checkedVRM ? '' : 'Please enter a valid reg');

            if (eventName === 'onBlur') {
                this.props.touchInput('vrm');
            }

            this.props.wrongPlateChange(undefined);

            if (checkedVRM && this.props.valuationForm.getIn(['collectedDetails', 'vrm', 'isTouched']) && eventName === 'onBlur') {
                let item = { vrm: value.toUpperCase(), mileage: '' };
                CRUDService.post('/i/vehicle/get', item, (vehicleDetails) => {

                    if (vehicleDetails.mileageFromMot) {
                        this.props.collectedDetailsChange('mileage', vehicleDetails.mileageFromMot);
                    }

                    this.props.vehicleDetailsChange(vehicleDetails);
                    let wrongPlate = vehicleDetails.hasNoInformation || vehicleDetails.error ? true : false || !vehicleDetails.isupportedForValuation;
                    this.props.wrongPlateChange(wrongPlate);

                    if(vehicleDetails.error){
                        toasts.createToast('error', vehicleDetails.error);
                    } else if (vehicleDetails.hasNoInformation) {
                        toasts.createToast('alert', "No information about this vehicle was found! (<a> Try again! </a>)", () => {
                            this.props.stepChange(0);
                        });
                    } else if (!vehicleDetails.isupportedForValuation) {
                        toasts.createToast('alert', "Sorry, we're not currently able to provide a valuation for this vehicle type! (<a> Try with another VRM! </a>)", () => {
                            this.props.stepChange(0);
                        });
                    }
                }), (err) => {
                    this.props.wrongPlateChange(true);

                    toasts.createToast('error', err.error, () => {
                        this.props.stepChange(0);
                    });
                };
            }
            break;
        default:
        }
    }

    handleInputBlur = (e, inputName) => {
        this.handleInput(e.target.value, inputName, 'onBlur');
    }

    handleInputChange = (e, inputName) => {
        this.handleInput(e.target.value, inputName, 'onChange');
    }

    nextStep = () => {
        this.handleInput(this.props.valuationForm.getIn(['collectedDetails', 'vrm', 'value']), 'vrm', 'onBlur');

        if (this.validateStep()) {
            this.props.stepChange(this.props.valuationForm.get('currentStep') + 1);
        } else {
            toasts.createToast('error', 'Please correct all the errors!')
        }
    }

    showError = (inputName) => {
        return this.props.valuationForm.getIn(['collectedDetails', inputName, 'isValid']) === false
            && this.props.valuationForm.getIn(['collectedDetails', inputName, 'isTouched']);
    }

    trackLinks = (e, name) => {
        hpitracking.track('NVF: Show Step: Valuation count exceeded | link clicked', {
            category: 'New Valuation Form',
            label: name
        }, {
            collectedEmail: this.props.valuationForm.getIn(['collectedDetails', 'email', 'value']),
            collectedIntention: this.props.valuationForm.getIn(['collectedDetails', 'intention', 'value']),
            collectedName: this.props.valuationForm.getIn(['collectedDetails', 'name', 'value']),
            collectedPostcode: this.props.valuationForm.getIn(['collectedDetails', 'postcode', 'value']),
            collectedVrm: this.props.valuationForm.getIn(['collectedDetails', 'vrm', 'value']),
            collectedMileage: this.props.valuationForm.getIn(['collectedDetails', 'mileage', 'value'])
        });
    }

    render() {

        let nextStepClass = classNames({
            'btn btn--square btn--orange': true,
            'btn--disabled': this.props.valuationForm.get('disableNext') || this.props.valuationForm.get('wrongPlate')
        });

        let notificationBox = <div>
            <div className='disclaimer'>
                <p className="notification">To keep our service free we have to limit the number of valuations each person can do.
                    If you're a very frequent user <a target='_blank'
                       onClick={ (e) => this.trackLinks(e, 'trade tools') }
                       href="https://www.cap-hpi.com/en/solutions/valuations">trade tools</a> may be more suitable.
                </p>
                <p>If you believe you're seeing this message in error, or have any questions
                        <a href="https://hpivaluations.com/contacts" target='_blank'
                           onClick={ (e) => this.trackLinks(e, 'contact us') }> contact us</a>.
                </p>
            </div>
        </div>;

        let vrmBox = <div>
            <input type="text" placeholder="Enter Reg" className="make-text-align-center" onBlur={(e) => this.handleInputBlur(e, 'vrm')}
                   ref={(input) => this.vrmInput = input}
                   onChange={(e) => { e.persist(); this.handleInputChange(e, 'vrm') }} />
            <div className="widget-react-embedded__step__error-message widget-react-embedded__step__error-message--registration"
                 style={{
                     'opacity': this.showError('vrm') ? 1 : 0
                 }}>{this.props.valuationForm.getIn(['collectedDetails', 'vrm', 'errrorMessage'])}</div>
            <button className={nextStepClass} onClick={this.nextStep}>Get a valuation</button>
        </div>;

        return (
            <div className="widget-react-embedded__step--registration">
                <div className="widget-react-embedded__wrapper--upper">
                    <div className="row">
                        <div className="small-12 columns make-text-align-center">
                            { this.showNotificationBox() ? notificationBox : vrmBox }
                        </div>
                    </div>
                </div>
                <div className="widget-react-embedded__wrapper--lower">
                    <div className="row">
                        <div className="small-12 columns make-text-align-center">
                            <div className="row">
                                <div className="small-7 small-offset-5 columns">
                                    <div className="logo make-text-align-right">
                                        <a href="https://hpivaluations.com" target='_blank'
                                           onClick={ (e) => this.trackLinks(e, 'logo') } className="logo">
                                            <img src="/assets/img/logos/dark_logo.png" alt="HPI Valuations" className="logo" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

// Inject action creators as actions
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        stepChange,
        collectedDetailsChange,
        vehicleDetailsChange,
        wrongPlateChange,
        disableNextStep,
        validateInput,
        touchInput
    }, dispatch);
}

// This component will have access to `initialstate().valuationWidget` through `this.props.valuationWidget`
function mapStateToProps(state) {
    return {
        valuationForm: state.valuationForm,
    };
}

// Connects the React component to the Redux store
export default connect(mapStateToProps, mapDispatchToProps)(StepRegistration);
