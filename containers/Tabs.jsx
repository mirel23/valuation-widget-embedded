/* eslint-disable */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { toObject } from 'immutable';
import classNames from 'classnames';
const hpitracking = HPI.tracking;

// Redux Actions
import {
    tabChange,
    explainerModalShow
} from '../actions';

class Tabs extends React.Component {

    getActiveTabLongName = (type) => {
        let tabs = [{
            activeTab: "dealer",
            nameLong: "Dealership Retail"
        }, {
            activeTab: "private",
            nameLong: "Private Sale"
        }, {
            activeTab: "trade",
            nameLong: "Trade-In"
        }];

        return tabs.filter((r) => {
           return r.activeTab ===  type;
        });
    }

    countAvailableTabs = () => {
        let i = 0;
        if (this.props.availableData.private) {
            i++;
        }
        if (this.props.availableData.dealership) {
            i++;
        }
        if (this.props.availableData.tradeIn) {
            i++;
        }
        return i;
    }

    onTabChange = (type) => {
        if (this.props.valuationWidget.get('activeTab') === type) {
            return;
        } else {
            let currentTab = this.getActiveTabLongName(this.props.valuationWidget.get('activeTab'))[0];
            let nextTab = this.getActiveTabLongName(type)[0];

            hpitracking.track('RP: Your valuation - tab change', {
                category: 'Report Page',
                label: 'Change from tab ' + currentTab.nameLong + " to tab " + nextTab.nameLong
            }, {
                userType: this.props.userType,
                currentTab: currentTab.nameLong,
                nextTab: nextTab.nameLong,
            });

            this.props.tabChange(type);
            this.props.explainerModalShow(false);
        }
    }

    render() {
        let tabStyle = {
            width: `${100 / this.countAvailableTabs()}%`
        }

        return (
            <ul className="tabs">
                { this.props.availableData.private &&
                <li className={ this.props.valuationWidget.get('activeTab') === "private" ? "active" : "" }
                    onClick={ () => this.onTabChange('private') } style={tabStyle}>
                    <span className="type">Private Sale</span>
                    <span className="border"></span>
                </li>
                }
                { this.props.availableData.dealership &&
                    <li className={ this.props.valuationWidget.get('activeTab') === "dealer" ? "active" : "" }
                        onClick={ () => this.onTabChange('dealer') } style={tabStyle}>
                        <span className="type">Forecourt</span>
                        <span className="border"></span>
                    </li>
                }
                { this.props.availableData.tradeIn &&
                    <li className={ this.props.valuationWidget.get('activeTab') === "trade" ? "active" : "" }
                        onClick={ () => this.onTabChange('trade') } style={tabStyle}>
                        <span className="type">Trade-In</span>
                        <span className="border"></span>
                    </li>
                }
            </ul>
        )
    }
}

// Inject action creators as actions
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        tabChange,
        explainerModalShow
    }, dispatch);
}

// This component will have access to `initialstate().valuationWidget` through `this.props.valuationWidget`
function mapStateToProps(state) {
    return {
        valuationWidget: state.valuationWidget,
    };
}

// Connects the React component to the Redux store
export default connect(mapStateToProps, mapDispatchToProps)(Tabs);
