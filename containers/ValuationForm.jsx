/* eslint-disable */
import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {toObject} from 'immutable';

// Components
import StepRegistration from './StepRegistration';
import StepIntention from './StepIntention';
import StepDetails from './StepDetails';
import StepGetValuation from './StepGetValuation';
import StepSpinner from './StepSpinner';
import StepValuationWidget from './StepValuationWidget';
// Consts
const hpitracking = HPI.tracking;
const CRUDService = HPI.CRUDService;
const toasts = HPI.toast;
const STEPS  = [{
    component: StepRegistration,
    description: 'Vehicle details',
    trackable: true
}, {
    component: StepIntention,
    description: 'User intention',
    trackable: true
}, {
    component: StepDetails,
    description: 'User details',
    trackable: true
}, {
    component: StepGetValuation,
    description: 'StepGetValuation',
    trackable: false
}, {
    component: StepSpinner,
    description: 'StepSpinner',
    trackable: false
}, {
    component: StepValuationWidget,
    description: 'Get Valuation',
    trackable: true
}];

// Redux Actions
import {
    stepChange,
    isMobileChange,
    partenerChange
} from '../actions';

class ValuationForm extends React.Component {

    componentDidMount() {
        this.props.partenerChange(this.props.partener);
        this.props.isMobileChange($(window).width() <= 767);

        $(window).on('resize', _.debounce(() => {
            this.props.isMobileChange($(window).width() <= 767);
        }, 500));

        this.handleTracking(STEPS[0].description);
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.valuationForm.get('currentStep') !== this.props.valuationForm.get('currentStep') &&
            STEPS[ this.props.valuationForm.get('currentStep') ].trackable) {
            this.handleTracking(STEPS[ this.props.valuationForm.get('currentStep') ].description);
        }
    }

    handleTracking = (step) => {
        hpitracking.track(`NVF: Show Step: ${step}`, {
            category: 'New Valuation Form',
            label: 'Step changed'
        }, {
            collectedEmail: this.props.valuationForm.getIn(['collectedDetails', 'email', 'value']),
            collectedIntention: this.props.valuationForm.getIn(['collectedDetails', 'intention', 'value']),
            collectedName: this.props.valuationForm.getIn(['collectedDetails', 'name', 'value']),
            collectedPostcode: this.props.valuationForm.getIn(['collectedDetails', 'postcode', 'value']),
            collectedVrm: this.props.valuationForm.getIn(['collectedDetails', 'vrm', 'value']),
            collectedMileage: this.props.valuationForm.getIn(['collectedDetails', 'mileage', 'value'])
        });
    }

    render() {

        const StepComponent = STEPS[ this.props.valuationForm.get('currentStep') ].component;
        let progress        = this.props.valuationForm.get('currentStep') * (100 / (STEPS.length - 2));
        let progressStyle   = {
            width: `${progress}%`
        };

        let widgetStyle = {};
        if (StepComponent === StepValuationWidget) {
            widgetStyle = {
                'backgroundImage': 'url(/assets/img/headers/hero_report.jpg)'
            };
        }

        return (
            <div className="widget-react-embedded__wrapper" style={widgetStyle}>
                { StepComponent !== StepValuationWidget &&
                    <div className="widget-react-embedded__progress">
                        <div className="progress" role="progressbar" tabIndex="0" aria-valuenow="50" aria-valuemin="0"
                             aria-valuetext={ `${progress} percent` } aria-valuemax="100">
                            <div className="meter" style={ progressStyle }></div>
                        </div>
                    </div>
                }

                <StepComponent />
            </div>
        )
    }
}

// Inject action creators as actions
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        stepChange,
        isMobileChange,
        partenerChange
    }, dispatch);
}

// This component will have access to `initialstate().valuationWidget` through `this.props.valuationWidget`
function mapStateToProps(state) {
    return {
        valuationForm: state.valuationForm,
    };
}

// Connects the React component to the Redux store
export default connect(mapStateToProps, mapDispatchToProps)(ValuationForm);
