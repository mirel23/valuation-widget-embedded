import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { toObject } from 'immutable';
import classNames from 'classnames';

import emailValidator from '../../../js/modules/emailValidator';

// Redux Actions
import {
    stepChange,
    collectedDetailsChange,
    disableNextStep,
    validateInput,
    touchInput
} from '../actions';

// Components
import PrevNextControls from '../components/PrevNextControls';

// Consts
const hpitracking = HPI.tracking;
const CRUDService = HPI.CRUDService;
const ValidationHelpers = HPI.ValidationHelpers;
const toasts = HPI.toast;

class StepDetails extends React.Component {

    componentDidUpdate(prevProps, prevState) {
        let disableNext = (this.props.valuationForm.getIn(['collectedDetails', 'name', 'isValid']) === false
            && this.props.valuationForm.getIn(['collectedDetails', 'name', 'isTouched'])) ||
            (this.props.valuationForm.getIn(['collectedDetails', 'email', 'isValid']) === false
            && this.props.valuationForm.getIn(['collectedDetails', 'email', 'isTouched'])) ||
            (this.props.valuationForm.getIn(['collectedDetails', 'postcode', 'isValid']) === false
            && this.props.valuationForm.getIn(['collectedDetails', 'postcode', 'isTouched'])) ||
            (this.props.valuationForm.getIn(['collectedDetails', 'phone', 'isValid']) === false
            && this.props.valuationForm.getIn(['collectedDetails', 'phone', 'isTouched']));

        this.props.disableNextStep(disableNext);
    }

    handleInputBlur = (e, inputName) => {
        this.handleInput(e.target.value, inputName, 'onBlur');
    }

    handleInputChange = (e, inputName) => {
        this.handleInput(e.target.value, inputName, 'onChange');
    }

    validateStep = () => {
        return this.props.valuationForm.getIn(['collectedDetails', 'name', 'isValid']) === true &&
            this.props.valuationForm.getIn(['collectedDetails', 'email', 'isValid']) === true &&
            this.props.valuationForm.getIn(['collectedDetails', 'postcode', 'isValid']) === true &&
            this.props.valuationForm.getIn(['collectedDetails', 'phone', 'isValid']) === true &&
            !this.props.valuationForm.get('wrongPlate');
    }

    nextStep = () => {
        this.handleInput(this.props.valuationForm.getIn(['collectedDetails', 'name', 'value']), 'name', 'onBlur');
        this.handleInput(this.props.valuationForm.getIn(['collectedDetails', 'email', 'value']), 'email', 'onBlur');
        this.handleInput(this.props.valuationForm.getIn(['collectedDetails', 'postcode', 'value']), 'postcode', 'onBlur');
        this.handleInput(this.props.valuationForm.getIn(['collectedDetails', 'phone', 'value']), 'phone', 'onBlur');

        if (this.validateStep()) {
            this.props.stepChange(this.props.valuationForm.get('currentStep') + 1);
        } else {
            toasts.createToast('error', 'Please correct all the errors!');

            this.props.valuationForm.get('collectedDetails').entrySeq().forEach((e) => {
                if (e[1].get('isValid') === false && e[1].get('errrorMessage') !== '') {
                    this.handleTracking('NVF: Show Error: User details', `${e[1].get('errrorMessage')}: Field: ${e[1].get('label')}`, e[1].get('label'),
                        e[1].get('value'), e[1].get('errrorMessage'), 'User details');
                }
            });
        }
    }

    prevStep = () => {
        this.props.stepChange(this.props.valuationForm.get('currentStep') - 1);
    }

    handleTracking = (details, label, fieldName, fieldValue, errorMessage, step) => {
        hpitracking.track(details, {
            category: 'New Valuation Form',
            label
        }, {
            fieldName,
            fieldValue,
            errorMessage,
            step,
            collectedEmail: this.props.valuationForm.getIn(['collectedDetails', 'email', 'value']),
            collectedIntention: this.props.valuationForm.getIn(['collectedDetails', 'intention', 'value']),
            collectedName: this.props.valuationForm.getIn(['collectedDetails', 'name', 'value']),
            collectedPostcode: this.props.valuationForm.getIn(['collectedDetails', 'postcode', 'value']),
            collectedVrm: this.props.valuationForm.getIn(['collectedDetails', 'vrm', 'value']),
            collectedMileage: this.props.valuationForm.getIn(['collectedDetails', 'mileage', 'value'])
        });
    }

    handleInput = (value, inputName, eventName) => {
        this.props.collectedDetailsChange(inputName, value);

        switch (inputName) {
        case 'name':
            this.props.validateInput('name', ValidationHelpers.validateUserName(value), ValidationHelpers.validateUserName(value) ? '' : 'Please enter your full name');
            eventName === 'onBlur' && this.props.touchInput('name');
            break;
        case 'email':
            this.props.validateInput('email', emailValidator.checkAddress(value), emailValidator.checkAddress(value) ? '' : 'Please enter a valid email');
            eventName === 'onBlur' && this.props.touchInput('email');
            break;
        case 'postcode':
            this.props.validateInput('postcode', ValidationHelpers.validatePostCode(value), ValidationHelpers.validatePostCode(value) ? '' : 'Please enter a valid postcode');
            eventName === 'onBlur' && this.props.touchInput('postcode');
            break;
        case 'phone':
            this.props.validateInput('phone', ValidationHelpers.validatePhone(value), ValidationHelpers.validatePhone(value) ? '' : 'Please enter a valid phone number');
            eventName === 'onBlur' && this.props.touchInput('phone');
            break;
        default:
        }
    }

    showError = (inputName) => {
        return this.props.valuationForm.getIn(['collectedDetails', inputName, 'isValid']) === false
            && this.props.valuationForm.getIn(['collectedDetails', inputName, 'isTouched']);
    }

    handleControl = (direction) => {
        if (direction === 'next') {
            this.nextStep();
        } else {
            this.prevStep();
        }
    }

    render () {

        let getInputClass = (inputName) => {
            return classNames({
                'invalid-input': this.showError(inputName)
            });
        };

        return (
            <div className="widget-react-embedded__step--details">
                <div className="widget-react-embedded__wrapper--upper">
                    <div className="row">
                        <div className="small-12 columns make-text-align-center">
                            <p className="title">Details
                                <span className="bottom-border-orange"></span>
                            </p>

                            <div className="input-group">
                                <input type="text" name="name" className={getInputClass('name')} placeholder="Full Name"
                                       onBlur={(e) => this.handleInputBlur(e, 'name')} onChange={(e) => this.handleInputChange(e, 'name')}
                                       value={this.props.valuationForm.getIn(['collectedDetails', 'name', 'value'])}/>
                                <i className="icon icon--name"></i>
                                {/*{ !this.props.valuationForm.get('isMobile') && <div className="widget-react-embedded__step__error-message" style={{*/}
                                    {/*'opacity': this.showError('name') ? 1 : 0*/}
                                {/*}}><p>Please enter your full name</p></div> }*/}
                                { <div className="widget-react-embedded__step__error-message" style={{
                                    'opacity': this.showError('name') ? 1 : 0
                                }}><p>Please enter your full name</p></div> }
                            </div>

                            <div className="input-group">
                                <input type="email" name="email" className={getInputClass('email')} placeholder="Email"
                                       onBlur={(e) => this.handleInputBlur(e, 'email')} onChange={(e) => this.handleInputChange(e, 'email')}
                                       value={this.props.valuationForm.getIn(['collectedDetails', 'email', 'value'])}/>
                                <i className="icon icon--email"></i>
                                { <div className="widget-react-embedded__step__error-message" style={{
                                    'opacity': this.showError('email') ? 1 : 0
                                }}><p>Please enter a valid email</p></div> }
                            </div>

                            <div className="input-group medium">
                                <input type="text" name="postcode" className={getInputClass('postcode')} placeholder="Postcode"
                                       onBlur={(e) => this.handleInputBlur(e, 'postcode')} onChange={(e) => this.handleInputChange(e, 'postcode')}
                                       value={this.props.valuationForm.getIn(['collectedDetails', 'postcode', 'value'])}/>
                                <i className="icon icon--postcode"></i>
                                { <div className="widget-react-embedded__step__error-message" style={{
                                    'opacity': this.showError('postcode') ? 1 : 0
                                }}><p>Please enter a valid postcode</p></div> }
                            </div>

                            <div className="input-group medium">
                                <input type="tel" name="phone" className={getInputClass('phone')} placeholder="Phone"
                                       onBlur={(e) => this.handleInputBlur(e, 'phone')} onChange={(e) => this.handleInputChange(e, 'phone')}
                                       value={this.props.valuationForm.getIn(['collectedDetails', 'phone', 'value'])}/>
                                <i className="icon-phone"></i>
                                { <div className="widget-react-embedded__step__error-message" style={{
                                    'opacity': this.showError('phone') ? 1 : 0
                                }}><p>Please enter a valid phone number</p></div> }
                            </div>
                        </div>
                    </div>
                </div>
                <div className="widget-react-embedded__wrapper--lower">
                    <div className="row">
                        <div className="small-12 columns make-text-align-center">
                            <PrevNextControls handleControl={this.handleControl}
                                              disableNext={this.props.valuationForm.get('disableNext') || this.props.valuationForm.get('wrongPlate')}
                                              hasNext="yes" hasPrev="yes" />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    }

// Inject action creators as actions
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        stepChange,
        collectedDetailsChange,
        disableNextStep,
        validateInput,
        touchInput
    }, dispatch);
}

// This component will have access to `initialstate().valuationWidget` through `this.props.valuationWidget`
function mapStateToProps(state) {
    return {
        valuationForm: state.valuationForm,
    };
}

// Connects the React component to the Redux store
export default connect(mapStateToProps, mapDispatchToProps)(StepDetails);
