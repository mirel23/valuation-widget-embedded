/* eslint-disable */
import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {toObject} from 'immutable';
import classNames from 'classnames';

// Components
import Tabs from './Tabs';
import ContentDealer from './ContentDealer';
import ContentPrivate from './ContentPrivate';
import ContentTradeIn from './ContentTradeIn';
import Upgrade from '../components/UpgradeOverlay';
import Thumbs from '../components/Thumbs';
import Feedback from '../components/Feedback';
import ConditionExplainer from './ConditionExplainer';
import Helpers from '../../../js/modules/Helpers';
import FormFeedback from '../components/FormFeedback';
import EstimateMileage from '../components/EstimateMileage';
// Consts
const hpitracking = HPI.tracking;
const CRUDService = HPI.CRUDService;
const toasts = HPI.toast;

// Redux Actions
import {
    privateConditionChange,
    dealerConditionChange,
    tradeInConditionChange
} from '../actions';

class ValuationWidget extends React.Component {
    state = {
        showFeedback: false,
        displayModal: false,
        direction: "",
        sentFeedback: Helpers.getCookie("fsent") ? JSON.parse(Helpers.getCookie("fsent")) : {},
        vrms: Helpers.getCookie("qv") ? JSON.parse(Helpers.getCookie("qv")) : {}
    }

    componentDidMount() {
        if (HPIDATA.showConversion && !this.state.vrms[this.props.report.vrm] && !HPIDATA.iframe) {
            setTimeout(() => {
                this.popUpGenerator();
            }, 15000);
        }
    }

    trackQuestionAnswer = (category, answer, label) => {

        hpitracking.track(answer, {
            category: "Report Survey: " + category,
            label: label ? label : this.props.report.rid
        });

        var vrms = this.state.vrms;

        vrms[this.props.report.vrm] = 1

        if (answer === 'Yes') {
            CRUDService.post("/api/leads/update", {
                reference: this.props.report.reference,
                requested_contact: true
            });
            this.setState({displayModal: true})
        }

        Helpers.setCookie("qv", JSON.stringify(vrms), 30)
    }

    closeModal = () => {
        this.setState({
            displayModal : false
        })
    }

    getRandomInt = (min, max) => {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    popUpGenerator = () => {
        let randomUI = this.getRandomInt(1, 2);
        let uiTypeClass = randomUI === 1 ? 'default' : 'modal';
        let uiType = randomUI === 1 ? 'Pop Up' : 'Modal';

        let questionsBuy = [{
            "id": 1,
            "title": "Get an offer",
            "content": "If you have to sell a car before you buy, would you like to get an offer on it from a dealer?"
        }, {
            "id": 2,
            "title": "Find a deal on a similar car",
            "content": "Would you like a dealer to contact you?"
        }];

        let questionsCurious = [{
            "id": 1,
            "title": "Get an offer",
            "content": "If you have to sell a car before you buy, would you like to get an offer on it from a dealer?"
        }];

        let questionsSell = [{
            "id": 1,
            "title": "Get an offer",
            "content": "Would you like to receive an offer on this car?"
        }, {
            "id": 3,
            "title": "Get an offer",
            "content": "Talk to a dealer who’s interested in buying this car?"
        }];

        let popupConstructor = (questions, label) => {
            let random = this.getRandomInt(1, questions.length);

            return toasts.createPopUp({
                type: uiTypeClass,
                title: questions[random - 1].title,
                content: questions[random - 1].content,
                options: {
                    closable: true,
                    closeOnConfirm: true
                },
                onConfirm: () => {
                    this.trackQuestionAnswer(uiType, "Yes", label + questions[random - 1].id);
                },
                onCancel: () => {
                    this.trackQuestionAnswer(uiType, "No", label + questions[random - 1].id);
                },
                onDismiss: () => {
                    this.trackQuestionAnswer(uiType, "Dismiss", label + questions[random - 1].id);
                }
            });
        }

        if (this.props.report.intention === "Buying") {
            popupConstructor(questionsBuy, "B");
        } else if (this.props.report.intention === "Selling") {
            popupConstructor(questionsSell, "S");
        } else {
            popupConstructor(questionsCurious, "C");
        }
    }

    rate = (direction) => {
        var self = this;
        var intention = !HPIDATA.iframe ? self.props.report.intention : "Embedded (HPI Check)";

        hpitracking.track(direction + ' rating', {
            category: 'Report rating',
            label: intention + " " + self.props.valuationWidget.get('activeTab')
        }, {
            direction: direction,
            intention: intention,
            tab: self.props.valuationWidget.get('activeTab'),
            reportId: self.props.report.rid
        });

        var feedbacks = this.state.sentFeedback;

        feedbacks[this.props.report.vrm] = 1;

        Helpers.setCookie("fsent", JSON.stringify(feedbacks), 30);

        this.setState({
            showFeedback: true,
            direction: direction,
            sentFeedback: JSON.parse(Helpers.getCookie("fsent"))
        });
    }

    closeFeedback = () => {
        this.setState({showFeedback: false});
    }


    feedbackSubmit = (e, message) => {
        e.preventDefault();

        if (message == "") {
            toasts.createToast('error', "Cannot send an empty message!");
        } else {
            var object = {
                direction: this.state.direction,
                name: this.props.report.collectedDetails.name,
                email: this.props.report.collectedDetails.email,
                intention: !HPIDATA.iframe ? this.props.report.intention : "Embedded (HPI Check)",
                tab: this.props.valuationWidget.get('activeTab'),
                reportId: this.props.report.rid,
                message: message
            }

            let self = this;

            CRUDService.post("/api/contact/feedback", object, function () {
                toasts.createToast('success', "Message was successfully sent!");
                self.setState({showFeedback: false});
            }, function () {
                toasts.createToast('error', "Something went wrong! Please try again!");
            });
        }
    }

    getSectionByTab(tab) {
        switch (tab) {
            case "private":
                return <ContentPrivate valuation={this.props.valuation} userType={this.props.report.type}
                                       showUpgrade={this.showUpgrade()}/>;
            case "dealer":
                return <ContentDealer valuation={this.props.valuation} userType={this.props.report.type}
                                      showUpgrade={this.showUpgrade()}/>;
            case "trade":
                return <ContentTradeIn valuation={this.props.valuation} userType={this.props.report.type}
                                       showUpgrade={this.showUpgrade()}/>;
        }
    }

    showUpgrade() {
        let result = false;
        if (this.props.report.type === "free" &&
            ((this.props.valuationWidget.get('activeTab') === "dealer" && ((this.props.report.availableData.private && this.props.report.availableData.tradeIn) ||
            (this.props.report.availableData.private && !this.props.report.availableData.tradeIn))) ||
            this.props.valuationWidget.get('activeTab') === "trade")) {
            result = true;
        }
        return result;
    }

    render() {

        let contentClass = classNames({
            'wrap-content': true,
            'modal': this.props.valuationWidget.get('showExplainerModal'),
            'threshold-msg': this.props.valuationWidget.get('lessThanThreshold')
        });

        return (
            <div>
                {this.state.displayModal && <FormFeedback close={this.closeModal} handleClose={this.closeModal}/>}

                <Tabs userType={this.props.report.type} availableData={this.props.report.availableData}/>

                <div
                    className={contentClass}>
                    <div className="outer">
                        <div className="inner active">
                            { this.getSectionByTab(this.props.valuationWidget.get('activeTab')) }

                            { this.showUpgrade() && <Upgrade availableData={this.props.report.availableData}
                                                             upgradelink={this.props.report.upgradelink}/> }


                            {HPIDATA.vehicle.currentMileage && <EstimateMileage mileage={HPIDATA.vehicle.currentMileage} mileageChangedByUser={this.props.valuationWidget.get('mileageChangedByUser')}/>}
                            {!this.state.sentFeedback[this.props.report.vrm] && <Thumbs rate={this.rate}/>}
                        </div>
                    </div>
                </div>
                <div className="condition-explainer"
                     style={{display: this.props.valuationWidget.get('showExplainerModal') ? 'block' : 'none'}}>
                    <ConditionExplainer userType={this.props.report.type}/>
                </div>

                {this.state.showFeedback &&
                <Feedback close={this.closeFeedback} submit={this.feedbackSubmit} direction={this.state.direction}/>}
            </div>
        )
    }
}

// Inject action creators as actions
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        privateConditionChange,
        dealerConditionChange,
        tradeInConditionChange
    }, dispatch);
}

// This component will have access to `initialstate().valuationWidget` through `this.props.valuationWidget`
function mapStateToProps(state) {
    return {
        valuationWidget: state.valuationWidget,
    };
}

// Connects the React component to the Redux store
export default connect(mapStateToProps, mapDispatchToProps)(ValuationWidget);
