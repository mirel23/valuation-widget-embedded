/* eslint-disable */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { toObject } from 'immutable';
import classNames from 'classnames';
import CommaChameloen from '../../../js/modules/CommaChameleon';
const hpitracking = HPI.tracking;

// Redux Actions
import {
    dealerConditionChange,
} from '../actions';

class ContentDealer extends React.Component {

    getActiveTabLongName = (type) => {
        let tabs = [{
            activeTab: "dealer",
            nameLong: "Dealership Retail"
        }, {
            activeTab: "private",
            nameLong: "Private Sale"
        }, {
            activeTab: "trade",
            nameLong: "Trade-In"
        }];

        return tabs.filter((r) => {
            return r.activeTab ===  type;
        });
    }

    showValue = (type) => {
        return this.props.showUpgrade ? "??,???" : CommaChameloen.commaString(this.props.valuation.dealership[type]);
    }

    onConditionChange = (condition, clickedFrom) => {
        if (this.props.valuationWidget.get('dealerCondition') === condition) {
            return;
        } else {
            let currentTab = this.getActiveTabLongName(this.props.valuationWidget.get('activeTab'))[0];

            hpitracking.track('RP: Your valuation - condition change', {
                category: 'Report Page',
                label: 'Tab ' + currentTab.nameLong + ': Change from condition ' + this.props.valuationWidget.get('dealerCondition') + " to condition " + condition + ": Condition changed from '" + clickedFrom + "'"
            }, {
                userType: this.props.userType,
                currentTab: currentTab.nameLong,
                currentCondition: this.props.valuationWidget.get('dealerCondition'),
                nextCondition: condition,
                changeSource: clickedFrom
            });

            this.props.dealerConditionChange(condition);
        }
    }

    render() {
        return (
            <div className="private-and-dealer-tab">
                <div className="value-wrapper">
                    <div className="value">
                        <span className="value-content">
                            <span>£{ this.showValue("low") }</span>
                            <span className="desc">Low</span>
                        </span>
                        <span className="seperator"></span>
                        <span className="value-content">
                            <span>£{ this.showValue("high") }</span>
                            <span className="desc">High</span>
                        </span>
                    </div>
                </div>
                <div className="logo">
                    <img src="/assets/img/logos/dark_logo_valuation_with_stamp.png" alt="hpivaluation logo" />
                </div>
            </div>
        )
    }
}

// Inject action creators as actions
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        dealerConditionChange
    }, dispatch);
}

// This component will have access to `initialstate().valuationWidget` through `this.props.valuationWidget`
function mapStateToProps(state) {
    return {
        valuationWidget: state.valuationWidget,
    };
}

// Connects the React component to the Redux store
export default connect(mapStateToProps, mapDispatchToProps)(ContentDealer);
