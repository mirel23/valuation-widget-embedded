import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {toObject} from 'immutable';

import { checkNested } from '../../../js/modules/Helpers';

// Components
import ValuationWidget from './ValuationWidget';

// Consts
const hpitracking = HPI.tracking;
const CRUDService = HPI.CRUDService;
const toasts = HPI.toast;

// Redux Actions
import {
    stepChange,
    tabChange
} from '../actions';

class StepValuationWidget extends React.Component {

    state = {
        hasValuation: checkNested((HPIDATA || null), 'report', 'availableData', 'hasValuation') && HPIDATA.report.availableData.hasValuation
    }

    componentWillMount() {
        let activeTab = '';
        let intention = HPIDATA.iframe ? 'Buying' : checkNested((HPIDATA || null), 'report', 'intention') && HPIDATA.report.intention; // Buying | Selling | Curious
        let hasPrivateValues = checkNested((HPIDATA || null), 'report', 'availableData', 'private') && HPIDATA.report.availableData.private;
        let hasDealerValues = checkNested((HPIDATA || null), 'report', 'availableData', 'dealership') && HPIDATA.report.availableData.dealership;
        let hasTradeValues = checkNested((HPIDATA || null), 'report', 'availableData', 'tradeIn') && HPIDATA.report.availableData.tradeIn;

        if (intention === 'Buying') {
            if (hasDealerValues) {
                activeTab = 'dealer';
            } else if (hasPrivateValues) {
                activeTab = 'private';
            } else if (hasTradeValues) {
                activeTab = 'trade';
            }
        } else {
            if (hasTradeValues) {
                activeTab = 'trade';
            } else if (hasPrivateValues) {
                activeTab = 'private';
            } else if (hasDealerValues) {
                activeTab = 'dealer';
            }
        }

        this.props.tabChange(activeTab);
    }

    get valuation() {
        if (this.state.hasValuation) {
            return <ValuationWidget report={ HPIDATA.report }
                                    valuation={ HPIDATA.valuation }/>
        } else {
            return <p>Sorry we're currently unable to provide a valuation for this vehicle. <a onClick={(e) => this.props.stepChange(0)}>Click to start again</a></p>
        }
    }
    
    render() {

        return (
            <div className="widget-react-embedded__step--valuation-widget">
                <div className="widget-react-embedded__wrapper--upper disable-top">
                    <div className="row">
                        <div className="small-12 columns make-text-align-center">
                            <div className="new-valuation-widget-react">
                                {this.valuation}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

// Inject action creators as actions
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        stepChange,
        tabChange
    }, dispatch);
}

// This component will have access to `initialstate().valuationWidget` through `this.props.valuationWidget`
function mapStateToProps(state) {
    return {
        valuationForm: state.valuationForm,
        valuationWidget: state.valuationWidget
    };
}

// Connects the React component to the Redux store
export default connect(mapStateToProps, mapDispatchToProps)(StepValuationWidget);
