import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {toObject} from 'immutable';

import CommaChameloen from '../../../js/modules/CommaChameleon';

// Redux Actions
import {
    stepChange,
    collectedDetailsChange,
    updateMileageChange
} from '../actions';

class MilesFormEmbedded extends React.Component {
    state = {
        mileageValue: CommaChameloen.commaString(this.props.mileage),
        milesChanged: false,
        mileage: ''
    }

    changeMiles = (e) => {
        let value = e.target.value.replace(/,/g, '');
        let regex = /^\d+$/;

        if(regex.test(value) || value === '') {
            let mileageValue = CommaChameloen.commaString(value);
            this.props.collectedDetailsChange('mileage', value);
            this.setState({
                mileageValue,
                milesChanged: true,
                mileage: value
            });
        }
    }

    submit = () => {
        this.props.updateMileageChange(true);
        // spinner step
        this.props.stepChange(4);
    }

    render() {

        let displaySubmitBtn;

        if (this.state.milesChanged && this.state.mileageValue !== '') {
            displaySubmitBtn = <button type="button" className="update-mileage-form__btn update-mileage-form__btn--save" onClick={(e) => { e.preventDefault(); this.submit() }}>Save</button>;
        } else {
            displaySubmitBtn = <button className="update-mileage-form__btn update-mileage-form__btn--disabled" type="button" onClick={(e) => { e.preventDefault(); this.mileageInput.focus() }}>Edit</button>;
        }

        return (
            <div className="text-center update-mileage-form">
                <input type="text" className="text-center" name="mileageWithComma"
                       value={this.state.mileageValue} ref={(input) => { this.mileageInput = input; }}
                       onChange={this.changeMiles}
                       onFocus={this.props.mileageFocus}
                       onBlur={this.props.mileageBlur}
                />
                <button className="update-mileage-form__btn update-mileage-form__btn--close" onClick={(e) => { e.preventDefault(); this.props.onClose() }} type="button">CLOSE</button> { displaySubmitBtn }
            </div>
        );
    }
}

// Inject action creators as actions
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        stepChange,
        collectedDetailsChange,
        updateMileageChange
    }, dispatch);
}

// This component will have access to `initialstate().valuationWidget` through `this.props.valuationWidget`
function mapStateToProps(state) {
    return {
        valuationForm: state.valuationForm,
    };
}

// Connects the React component to the Redux store
export default connect(mapStateToProps, mapDispatchToProps)(MilesFormEmbedded);
