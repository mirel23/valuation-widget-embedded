import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { toObject } from 'immutable';
import classNames from 'classnames';

// Redux Actions
import {
    stepChange
} from '../actions';

// Components
import VehicleInformation from '../components/VehicleInformation';

// Consts
const hpitracking = HPI.tracking;
const CRUDService = HPI.CRUDService;
const toasts = HPI.toast;

class StepGetValuation extends React.Component {

    changeVehicle = () => {
        this.props.stepChange(0);
    }

    nextStep = () => {
        if (this.props.valuationForm.get('wrongPlate') === false) {
            this.props.stepChange(this.props.valuationForm.get('currentStep') + 1);
        } else {
            toasts.createToast('error', 'Please correct all the errors!');
        }
    }

    render() {

        let nextStepClass = classNames({
            'btn btn--round btn--orange': true,
            'btn--disabled': this.props.valuationForm.get('wrongPlate') !== false
        });

        return (
            <div className="widget-react-embedded__step--get-valuation">
                <div className="widget-react-embedded__wrapper--upper">
                    <div className="row">
                        <div className="small-12 columns make-text-align-center">
                            <VehicleInformation changeVehicle={this.changeVehicle} vehicleDetails={this.props.valuationForm.get('vehicleDetails')} wrongPlate={this.props.valuationForm.get('wrongPlate')}/>
                            <p className="widget-react-embedded__step--get-valuation__disclaimer">
                                <small>We're able to keep our service free by sharing the information you provide with a limited
                                    number of carefully selected third parties in the automotive sector. By proceeding you are
                                    giving your permission for your details to be used in this way and accepting our <a
                                        target='_blank' href="/terms">Terms & Conditions</a>.
                                </small>
                            </p>
                        </div>
                    </div>
                </div>
                <div className="widget-react-embedded__wrapper--lower">
                    <div className="row">
                        <div className="small-12 columns make-text-align-center">
                            <button className={nextStepClass} onClick={this.nextStep}>Get your Valuation</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

// Inject action creators as actions
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        stepChange,
    }, dispatch);
}

// This component will have access to `initialstate().valuationWidget` through `this.props.valuationWidget`
function mapStateToProps(state) {
    return {
        valuationForm: state.valuationForm,
    };
}

// Connects the React component to the Redux store
export default connect(mapStateToProps, mapDispatchToProps)(StepGetValuation);
